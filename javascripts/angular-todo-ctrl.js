app.controller('todoCtrl', ['$scope', '$timeout', function($scope, $timeout) {
  init = function() {
    $scope.form = {};
    $scope.isShow = (localStorage.getItem('isShow')) ? angular.fromJson(localStorage.getItem('isShow')) : '';

    if (!localStorage.getItem('todos')) localStorage.setItem('todos', '[]');
    $scope.todos = angular.fromJson(localStorage.getItem('todos'));
  }

  $scope.add = function() {
    $scope.todos.push({
      id: generateId(),
      title: $scope.form.title,
      description: $scope.form.description,
      created_at: timestamp(),
      updated_at: timestamp(),
      status: false,
      physical: true
    });

    $scope.clearForm();
  }

  $scope.remove = function(item) {
    var index = $scope.todos.indexOf(item);
    $scope.todos.splice(index, 1);
  }

  $scope.changeStatus = function(item) {
    var index = $scope.todos.indexOf(item),
        temp = angular.copy($scope.isShow);

    $scope.todos[index].status = !$scope.todos[index].status;
    saveToLocalStorage();
    $scope.isShow = 'not-show';
    $timeout(function () { $scope.isShow = temp; });
  }

  $scope.completed_count = function() {
    return $scope.todos.filter(function(value) { return value.status }).length;
  }

  $scope.clearForm = function() {
    $scope.form = {
      id: generateId()
      // created_at: timestamp(),
      // updated_at: timestamp()
    };
    $scope.temp_index = undefined;
  }

  $scope.editForm = function(item) {
    $scope.form.id = item.id;
    $scope.form.title = item.title;
    $scope.form.description = item.description;
    $scope.form.created_at = item.created_at;
    $scope.form.updated_at = item.updated_at;

    var index = $scope.todos.indexOf(item);
    $scope.temp_index = index;
  }

  $scope.update = function() {
    if (!$scope.temp_index && $scope.temp_index != 0) return;

    $scope.todos[$scope.temp_index].title = $scope.form.title;
    $scope.todos[$scope.temp_index].description = $scope.form.description;
    $scope.todos[$scope.temp_index].updated_at = timestamp();
    $scope.temp_index = undefined;
    saveToLocalStorage();
  }

  $scope.show = function(arg) {
    $scope.isShow = arg;
    saveToLocalStorage();
  }

  $scope.showTodoList = function() {
    if ($scope.isShow == 'not-show') return {};
    if (typeof $scope.isShow == 'string') return $scope.todos;
    if ($scope.isShow) return $scope.todos.filter(function(value) { return value.status });
    return $scope.todos.filter(function(value) { return !value.status });
  }

  generateId = function() {
    return moment(new Date()).format('x');
  }

  timestamp = function() {
    return moment(new Date()).format('DD-MMM-YYYY HH:mm:ss');
  }

  saveToLocalStorage = function() {
    localStorage.setItem('todos', angular.toJson($scope.todos));
    localStorage.setItem('isShow', $scope.isShow);
  }

  $scope.$watch('todos.length', function(newVal, oldVal) {
    saveToLocalStorage();
  });

  init();
}]);
